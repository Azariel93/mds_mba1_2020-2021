﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Goal : MonoBehaviour
{
    private Rigidbody2D rigidbody2D;

    public GameObject side;
    public List<GameObject> targets;

    private DateTime _nextChangeTime = DateTime.Now;

    public GameObject goalAnim;
    
    private void Start() {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.CompareTag("Ball")) {
            StartCoroutine("pauseGame");
        }
    }

    private void resetActors() {
        foreach (var t in targets) {
            if (t.gameObject.CompareTag("Ball")) {
                var position = t.transform.position;
                position.x = 0f;
                position.y = 0f;
                t.transform.position = position;
                t.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                t.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0f;
            } else {
                resetPos(findItemName("Girl"), "left");
                resetPos(findItemName("Trump"), "right");
            }
        }
    }

    private GameObject findItemName(String name) {
        GameObject obj = null;
        foreach (var t in targets) {
            if (t.gameObject.name == name) {
                obj = t.gameObject;
            }
        }
        return obj;
    }

    private void resetPos(GameObject obj, String side) {
        var position = obj.transform.position;
        if (side == "left") {
            position.x = -1f;
        } else {
            position.x = 1f;
        }
        position.y = 0f;
        obj.transform.position = position;
    }

    private void addPoint() {
        AnimateGirl girl = findItemName("Girl").GetComponent<AnimateGirl>();
        AnimateTrump trump = findItemName("Trump").GetComponent<AnimateTrump>();
        if (side.gameObject.CompareTag("Right")) {
            girl.AddPoint();
        } else if (side.gameObject.CompareTag("Left")) {
            trump.AddPoint();
        }
        if (girl.getScore() > 4 || trump.getScore() > 4) {
            EndGame();
            girl.ResetScore();
            trump.ResetScore();
        }
    }

    private void EndGame() {
        
    }

    private IEnumerator pauseGame() {
        addPoint();
        resetActors();

        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(3f);

        if (side.gameObject.CompareTag("Right")) {
            GameObject anim = Instantiate(goalAnim, new Vector3(-1f,0,0), Quaternion.identity);
            anim.SetActive(true);
            anim.GetComponent<ParticleSystem>().Play();
        } else {
            GameObject anim = Instantiate(goalAnim, new Vector3(1f,0,0), Quaternion.identity);
            anim.SetActive(true);
            anim.GetComponent<ParticleSystem>().Play();
        }

        Time.timeScale = 1;
    }
}
