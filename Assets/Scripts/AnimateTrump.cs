﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(Animator), typeof(Rigidbody2D))]
public class AnimateTrump : MonoBehaviour
{
    [Tooltip("Vitesse max en unités par secondes")]
    public int MaxSpeed = 2;

    // Autres scripts
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private Rigidbody2D rigidbody2D;

    // variables de mon instance
    private Vector3 speed;
    private int score;

    // statics
    private static readonly int Speed = Animator.StringToHash("Speed");
    private static readonly int GoingUp = Animator.StringToHash("GoingUp");
    private static readonly int GoingDown = Animator.StringToHash("GoingDown");
    private static readonly int GoingLeft = Animator.StringToHash("GoingLeft");
    private static readonly int GoingRight = Animator.StringToHash("GoingRight");

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        ResetScore();
    }

    void FixedUpdate()
    {
        var maxDistancePerFrame = MaxSpeed;
        Vector3 move = Vector3.zero;

        if (Input.GetKey(KeyCode.D))
        {
            move += Vector3.right * maxDistancePerFrame;
        }
        else if (Input.GetKey(KeyCode.Q))
        {
            move += Vector3.left * maxDistancePerFrame;
        }

        if (Input.GetKey(KeyCode.Z))
        {
            move += Vector3.up * maxDistancePerFrame;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            move += Vector3.down * maxDistancePerFrame;
        }

        animator.SetBool(GoingUp, Input.GetKey(KeyCode.Z));
        animator.SetBool(GoingDown, Input.GetKey(KeyCode.S));
        animator.SetBool(GoingLeft, Input.GetKey(KeyCode.Q));
        animator.SetBool(GoingRight, Input.GetKey(KeyCode.D));

        animator.SetFloat(Speed, move.magnitude * 10f);
        rigidbody2D.velocity = move;
    }

    public void ResetScore() {
        this.score = 0;
        for(int i = 0; i < 5; i++) {
            this.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public int getScore() {
        return this.score;
    }

    public void AddPoint() {
        this.score ++;
        if (this.transform.childCount > this.score - 1) {
            this.transform.GetChild(this.score - 1).gameObject.SetActive(true);
        }
    }
}